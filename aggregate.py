import pymongo
import urllib2
import sys
import json
import datetime
import unicodedata

_verbose_ = False
sqlSchema = "michel"
mongodbURL = 'mongodb://52.17.64.91:27017'
client = pymongo.MongoClient(mongodbURL)
conn = client.admin.authenticate('bf', 'alls33ing1', mechanism='SCRAM-SHA-1')

dateFrom = "2015081000" #YYYYMMDDHH
dateTo   = "2017010100"

# Target database
testColl = client.test_mapreduce
# Collection with wiki references
wikiRefColl = testColl.wikiref_rm 
# Collection to store Wikipedia page views
wikimediaColl = testColl.grok

aggregator = wikiRefColl.aggregate([
{
    "$project" : {
    	"imdb" : 1,
    	"date_retrieved": 1,
    	"date_updated": 1,
    	"date_released": 1,
        "title_from_omdb" : "$title_from_omdb",
        "wikiref" : [ "$wikiref_1",  "$wikiref_2", "$wikiref_3", "$wikiref_4", "$wikiref_5" ]
    }
}, {
    "$unwind" : {
        "path" : "$wikiref"
    }
}
# , {
#     "$match" : {
#         "wikiref": { "$not" : { "$eq" : None } }
#     }
# }
])
# Exanple URL for Wikimedia data
# https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/The_General_%281927_film%29/daily/2015080100/2016020200
wikiQueryURL = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/%s/daily/%s/%s"
res = {}
for a in aggregator:
	date_updated = datetime.datetime.utcnow()
	try:
		if (not res.has_key(a['title_from_omdb'])):
			if _verbose_: print a['title_from_omdb']
			res[a['title_from_omdb']] = a
			try:
				# .encode('utf-8').strip()
				wikiRef = a['wikiref']
				if (wikiRef != None):
					wikiRefconverted = unicodedata.normalize('NFD', wikiRef).encode('ascii', 'ignore')
					query = wikiQueryURL % (wikiRefconverted, dateFrom, dateTo)
					try:
						wikiConn = urllib2.urlopen(query)
					except urllib2.HTTPError:
						print 'Error 404:', query
					try:
						data = json.loads(wikiConn.readlines()[0])
						wikiConn.close()
					except TypeError:
						print 'No results for', query
						wikiConn.close()
					# Have the data, now process it
					daily_views = {}
					for d in data['items']:
						tmpKey = "%s-%s-%s" % (d['timestamp'][:4], d['timestamp'][4:6], d['timestamp'][6:8])
						try:
							# Already has the month key
							daily_views[d['timestamp'][:6]][tmpKey] = d['views']
						except KeyError:
							# Need to create a new month key
							daily_views[d['timestamp'][:6]] = {}
							# print daily_views
							daily_views[d['timestamp'][:6]][tmpKey] = d['views']
					res[a['title_from_omdb']]['daily_views'] = daily_views
					try:
						res[a['title_from_omdb']]['date_updated']
					except KeyError:
						res[a['title_from_omdb']]['date_update'] = date_updated.strftime("%Y%m%d%H%M%S")
					# try:
					entry = wikimediaColl.find_one({'title_from_omdb' : a['title_from_omdb']})
					# print entry
					try:
						a['date_retrieved'] = entry['date_retrieved']
						if (entry['date_retrieved'] == None): a['date_retrieved'] = date_updated.strftime("%Y%m%d%H%M%S")
					except KeyError:
						a['date_retrieved'] = date_updated.strftime("%Y%m%d%H%M%S")
					mergedDic = entry['daily_views'].copy()
					mergedDic.update(a['daily_views'])
					a['daily_views'] = mergedDic
					wikimediaColl.delete_one({'title_from_omdb' : a['title_from_omdb']})
					post_id = wikimediaColl.insert(res[a['title_from_omdb']])
			except AttributeError:
				# Some wikirefs are 'None'. Skip those.
				pass

	except KeyError:
		print 'KeyError for', a

print len(res)
client.close()