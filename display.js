var fetchLog = function(fName){
    var logArr = fName.split('/');
    $('#logName')[0].innerText = 'Filename: '+ logArr[logArr.length-1];
    getData(fName).then( function(res) {
        processData(res).then(function (rr) {
        $(".log-results").append(rr);  
        $(".search").keyup(function () {
            var searchTerm = $(".search").val();
            var listItem = $('.results tbody').children('tr');
            var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
            
          $.extend($.expr[':'], {'containsi': function(elem, i, match, array){
                return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            }
          });
            
          $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
            $(this).attr('visible','false');
          });

          $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
            $(this).attr('visible','true');
          });

          var jobCount = $('.results tbody tr[visible="true"]').length;
            $('.counter').text(jobCount + ' item');

          if(jobCount == '0') {$('.no-result').show();}
            else {$('.no-result').hide();}
                  });

            }).then( function (){
              $("#res tr:even").addClass("odd");
              $("#res tr:not(.odd)").hide();
              $("#res tr:first-child").show();
            
              $("#res tr.odd").click(function(){
                $(this).next("tr").toggle();
                $(this).find(".arrow").toggleClass("up");
            });

            }) 
        .fail( function (e){
            console.error(e);
        });

    });
}

var getData = function(fname){
    return $.ajax({
    url: fname
    });

}

var processData = function (res) {
    var deferred = new $.Deferred();
    var logdata = {};
    var count = 0;
    var table = "";
        for (var i in res){
        // if (i !== 'null'){
            count = count + 1;
            console.log(i);
            var imdb = null;
            var errorNum = null;
            var warnLen = 0;
            var errLen = 0;
            var extraInfo = '';
            try {
                imdb  = res[i]['IMDB']
                errLen = res[i]['Errors'].length;
                warnLen = res[i]['Warnings'].length;
                errorNum = res[i]['ErrorNumber'];
            } catch (err){
                //
            }
            if (errLen > 0){
              extraInfo = "<h4>Errors:</h4><ul>";
              for (var j = 0; j < errLen; j++){
                extraInfo = extraInfo + '<li>' + res[i]['Errors'][j] + '</li>';
              }
              extraInfo + extraInfo +'</ul>'
            }
            if (warnLen > 0){
              extraInfo = extraInfo + "<h4>Warnings:</h4><ul>";
              for (var j = 0; j < warnLen; j++){
                extraInfo = extraInfo + '<li>' + res[i]['Warnings'][j] + '</li>';
              }
              extraInfo + extraInfo +'</ul>'
            }
            var tabRow = '<tr><th scope="row">' + count+'</th>';
            tabRow = tabRow + '<td>' + i +'</td>';
            tabRow = tabRow + '<td>' + imdb +'</td>';
            tabRow = tabRow + '<td>' + errLen +'</td>';
            tabRow = tabRow + '<td>' + warnLen +'</td>';
            tabRow = tabRow + '<td>' + errorNum +'</td></tr>';
            tabRow = tabRow + '<tr><td colspan="6">'+extraInfo+'</td></tr>'
            table = table + tabRow;       
        // }
    }
        deferred.resolve(table);
        // console.log(table);
     return deferred.promise();
     // var container = $('.log-results');
     // for (var k in logdata){
     //    if (!(k.includes('local.') || k.includes('admin.'))){
     //        var newDiv = document.createElement('div');
     //        addChart($(newDiv), logdata[k], k);
     //        container.append(newDiv);
     //    }
     // }
};