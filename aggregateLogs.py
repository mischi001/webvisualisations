import sys, os
import json

errorLog = {} ## This is bad practice

def getFileList(nameRoot, dateStr, path):
	flist = filter(lambda x: nameRoot+dateStr in x and x.endswith('.log'), os.listdir(path)) 
	return flist

def writeJsonLog(log, prefix):
	from datetime import datetime
	dateStr = datetime.strftime(datetime.now(), "%Y-%m-%d")
	filename = prefix + dateStr + '.json'
	fout = open(filename, 'w')
	json.dump(log, fout)
	fout.close()

def processLogFile(filename):
	import re
	global errorLog
	f = open(filename)
	tmpImdb = None
	tmpId = None
	warnArr=[]
	errArr=[]
	tmpLabel = None
	tmpError = None
	lines = f.readlines()
	for l in lines: ##f.readline():
		# print l
		if 'imdb' in l:
			p = re.compile(ur'imdb:.(\d*)')
			tmpImdb = p.match(l).group(1)
		elif 'MKGO_id:' in l:
			# if tmpId != None:
			# 	tmpId = None
			# 	tmpImdb = None
			# 	warnArr=[]
			# 	errArr=[]
			p = re.compile('MKGO_id:.(.*)')
			tmpId = p.match(l).group(1)
		elif l.startswith('error_'):
			p = re.compile(ur'error_(\d*):(.*)')
			tmpLabel = p.match(l).group(1)
			tmpError = p.match(l).group(2)
			# Now write all collected information to the dictionary
			# print '>>', tmpId, '//', tmpImdb
		elif '--------' in l:
		 	try:
				errorLog[tmpId] = {'Errors': errArr, 'IMDB':tmpImdb, 'Warnings': warnArr, 'ErrorNumber': [tmpLabel,tmpError]}
			except KeyError:
				print 'KEY ERROR', tmpId
			tmpImdb = None
			tmpId = None
			warnArr=[]
			errArr=[]	
			tmpLabel = None
			tmpError = None
		else:
			if l.lower().startswith('warning'):
				warnArr.append(l)
			else:
				errArr.append(l)	

if __name__ == "__main__":	
	from datetime import datetime, timedelta
	path = '/home/ruijie/logs/'
	today = datetime.strftime(datetime.now(), "%Y%m%d")
	fList = getFileList('clean_predict_', today, path)
	print fList
	if fList == []:
		print 'No files matched!'
		sys.exit(0)

	for f in fList:
		processLogFile(path + f)
		break
	writeJsonLog(errorLog, 'parsed/summary-')